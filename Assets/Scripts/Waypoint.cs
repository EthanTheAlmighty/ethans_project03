﻿using UnityEngine;
using System.Collections;

public enum MovementType
{
    STRAIGHT_LINE, 
    BEZIER_CURVE,
    WAIT,
    ROTATION_CHAIN
}

public enum FacingType
{
    FREE_MOVEMENT,
    LOOK_AND_RETURN,
    LOOK_CHAIN,
    FORCED_LOCATION
}

public enum  EffectsType
{
    CAMERA_SHAKE,
    SPLATTER,
    FADE
}

[System.Serializable]
public class Waypoint {

    public WaypointTypes waypointTypes;

    #region "Movement"
    public MovementType movementTypes;

    //Straight Line
    public Transform endpoint;
    public float timeToGetThere;

    //Wait
    public float waitTime;

    //Bezier curve
    public float bezierMoveTime;
    public Transform bezierEndPoint;
    public Transform bezierCurvePoint;

    //Rotation Chain
    public Transform[] rotatePoints;
    public float[] rotateSpeeds;
    #endregion

    # region "Facing"
    public FacingType facingTypes;

    //free movement
    public float freeLookTime;

    //look and return
    public Transform lookDestination;
    public float lookTime;
    public float travelToTime;
    public float travelBackTime;

    //look chain
    public Transform[] lookPoints;
    public float[] lookTimes;

    //forced location
    public Transform forcedLookTarget;
    public float forcedLookTime;

    #endregion

    #region "Effects"
    public EffectsType specialEffectsType;

    //camera shake
    public float shakeTime;
    public float shakeIntensity;

    //splatter
    public float splatterTime;
    public bool fadeInAndOut;
    public float splatterFadeTime;

    //Fade
    public bool fadeOut;
    public float fadeTime;

    #endregion
}
