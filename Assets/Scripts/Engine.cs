﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public enum WaypointTypes
{
    MOVEMENT,
    FACING,
    EFFECTS
}

public class Engine : MonoBehaviour {
    //store all the way points
    public Waypoint[] waypoints;

    IEnumerator Start()
    {
        for(int i = 0; i < waypoints.Length; i++)
        {
            switch(waypoints[i].waypointTypes)
            {
                case WaypointTypes.MOVEMENT:
                    yield return StartCoroutine("MovementEngine", i);
                    break;
                case WaypointTypes.FACING:
                    yield return StartCoroutine("FacingEngine", i);
                    break;
                case WaypointTypes.EFFECTS:
                    yield return StartCoroutine("EffectsEngine", i);
                    break;
            }
        }
    }//start

    IEnumerator MovementEngine(int index)
    {
        switch (waypoints[index].movementTypes)
        {
            case MovementType.STRAIGHT_LINE:
                Transform endpoint = waypoints[index].endpoint;
                float straightTime = waypoints[index].timeToGetThere;

                float elapsedTime = 0.0f;
                Transform startPos = this.transform;
                 while(elapsedTime < straightTime)
                {
                    transform.position = Vector3.Lerp(startPos.position, endpoint.position, (elapsedTime / straightTime));
                    elapsedTime += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                break;
            case MovementType.BEZIER_CURVE:
                Transform start = this.transform;
                float bezierMoveTime = waypoints[index].bezierMoveTime;
                Transform bezierEndPoint = waypoints[index].bezierEndPoint;
                Transform bezierCurvePoint = waypoints[index].bezierCurvePoint;
                //i thought i had this down but the Lerping wasn't working quite right
                float t = 0.0f;

                while(t < 1.0f)
                {
                    transform.position = Vector3.Lerp(start.position, QuadraticBezier(start.position, bezierCurvePoint.position, bezierEndPoint.position, t), t);
                }

                break;
            case MovementType.ROTATION_CHAIN:
                //i thought about tackling this, but holy shit fuck that noise
                break;
            case MovementType.WAIT:
                yield return new WaitForSeconds(waypoints[index].waitTime);
                break;
        }
        yield return null;
    }

    IEnumerator FacingEngine(int index)
    {
        switch (waypoints[index].facingTypes)
        {
            case FacingType.FREE_MOVEMENT:
                break;
            case FacingType.LOOK_AND_RETURN:
                Quaternion begin = transform.rotation;
                Transform lookTarget = waypoints[index].lookDestination;
                float stay = waypoints[index].lookTime;
                float to = waypoints[index].travelToTime;
                float from = waypoints[index].travelBackTime;
                GetComponent<MouseLook>().enabled = false;

                break;
            case FacingType.LOOK_CHAIN:
                GetComponent<MouseLook>().enabled = false;
                break;
            case FacingType.FORCED_LOCATION:
                GetComponent<MouseLook>().enabled = false;
                Transform target = waypoints[index].forcedLookTarget;
                float forcedTime = waypoints[index].forcedLookTime;
                Quaternion start = transform.rotation;
                Quaternion targetDir = Quaternion.LookRotation(target.position - transform.position);
                float step = 0.0f;
                //lerp rotations
                while (step < 1.0f)
                {
                    Quaternion.Lerp(start, targetDir, step);
                    step += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                //wait at target
                yield return new WaitForEndOfFrame();
                break;
        }
        GetComponent<MouseLook>().enabled = true;
        yield return null;
    }

    IEnumerator EffectsEngine(int index)
    {
        //need access to the splat and fading images on canvas to do things with
        GameObject faderObject = GameObject.Find("fader");
        Image fader = faderObject.GetComponent<Image>();
        GameObject splatObject = GameObject.Find("fader");
        Image splat = splatObject.GetComponent<Image>();
        switch (waypoints[index].specialEffectsType)
        {
            case EffectsType.CAMERA_SHAKE:
                float shakeTime = waypoints[index].shakeTime;
                float shakeIntensity = waypoints[index].shakeIntensity;
                float step = 0.0f;
                while(step < shakeTime)
                {
                    Camera.main.transform.localPosition = Random.insideUnitSphere * shakeIntensity;
                    step += Time.deltaTime;
                    yield return new WaitForEndOfFrame();
                }
                break;
            case EffectsType.FADE:
                bool fadeOut = waypoints[index].fadeOut;
                float fadeTime = waypoints[index].fadeTime;
                if(fadeOut)
                {
                    fader.CrossFadeAlpha(255, fadeTime, true);//not sure about that true bit
                }
                else if (!fadeOut)
                {
                    fader.color = new Color(0, 0, 0, 255);
                    fader.CrossFadeAlpha(0, fadeTime, true);
                }
                yield return new WaitForSeconds(fadeTime);
                break;
            case EffectsType.SPLATTER:
                float splatterTime = waypoints[index].splatterTime;
                bool fadeInAndOut = waypoints[index].fadeInAndOut;
                float splatterFadeTime = waypoints[index].splatterFadeTime;

                if(fadeInAndOut)
                {
                    splat.CrossFadeAlpha(255, splatterTime, true);
                    yield return new WaitForSeconds(splatterTime);
                    splat.CrossFadeAlpha(0, splatterFadeTime, true);
                    yield return new WaitForSeconds(splatterFadeTime);
                }
                else
                {
                    splat.CrossFadeAlpha(255, splatterTime, true);
                    yield return new WaitForSeconds(splatterTime);
                    splat.color = new Color(0, 0, 0, 0);
                }
                break;
        }
        yield return null;
    }

    Vector3 QuadraticBezier(Vector3 Start, Vector3 Control, Vector3 End, float t)
    {
        return (((1 - t) * (1 - t)) * Start) + (2 * t * (1 - t) * Control) + ((t * t) * End);
    }
}
