﻿using UnityEngine;
using System.Collections;
using UnityEditor;

[CustomPropertyDrawer(typeof(Waypoint))]
public class WaypointDrawer : PropertyDrawer
{
    Waypoint thisWaypoint;
    float extraHeight = 75f;
    const float EXTRA_HEIGHT = 75f;
    const float LINE_HEIGHT = 18f;
    const float INDENT = 22f;

    public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
    {
        EditorGUI.BeginProperty(position, label, property);
        #region"using the editor"

        //all the editor stuff in here

        //display the type of waypoint

        //setup a position to display the variable
        Rect waypointPos = new Rect(position.x, position.y, position.width, LINE_HEIGHT);
        //get the variable off the script
        SerializedProperty waypointType = property.FindPropertyRelative("waypointTypes");
        //Display the property in the editor
        EditorGUI.PropertyField(waypointPos, waypointType);
        
        switch((WaypointTypes)waypointType.enumValueIndex)
        {
            case (WaypointTypes.MOVEMENT):
                
                Rect subtypePos = new Rect(position.x + INDENT, position.y + LINE_HEIGHT, position.width - INDENT, LINE_HEIGHT);
                //make sure you get the variable here and not the type or the enum
                SerializedProperty movementType = property.FindPropertyRelative("movementTypes");
                EditorGUI.PropertyField(subtypePos, movementType);
                switch((MovementType)movementType.enumValueIndex)
                {
                    #region "movement"
                    case (MovementType.STRAIGHT_LINE):
                        Rect endpointPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect moveTimePos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                        SerializedProperty endPoint = property.FindPropertyRelative("endpoint");
                        SerializedProperty moveTime = property.FindPropertyRelative("timeToGetThere");

                        EditorGUI.PropertyField(endpointPos, endPoint);
                        EditorGUI.PropertyField(moveTimePos, moveTime);
                        break;
                    case (MovementType.ROTATION_CHAIN):
                        Rect rotatePointsPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect rotatePointsSizePos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect rotateSpeedsPos = new Rect(position.x + 2 * INDENT, position.y + 4 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                        Rect rotatePointsElementsPos = new Rect(position.x + 3 * INDENT, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT)/2, LINE_HEIGHT);
                        Rect rotateSpeedsElementsPos = new Rect((position.x + 3 * INDENT) + (position.width - 3 * INDENT) / 2, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT)/2, LINE_HEIGHT);

                        SerializedProperty rotateSpeeds = property.FindPropertyRelative("rotateSpeeds");

                        //display array
                        SerializedProperty rotatePoints = property.FindPropertyRelative("rotatePoints");
                        if (rotatePoints.isExpanded)
                        {
                            EditorGUI.PropertyField(rotatePointsSizePos, rotatePoints.FindPropertyRelative("Array.size"));
                            rotateSpeeds.arraySize = rotatePoints.arraySize;

                            for (int i = 0; i < rotatePoints.arraySize; i++)
                            {
                                EditorGUI.PropertyField(rotatePointsElementsPos, rotatePoints.GetArrayElementAtIndex(i));
                                EditorGUI.PropertyField(rotateSpeedsElementsPos, rotateSpeeds.GetArrayElementAtIndex(i));
                                rotatePointsElementsPos.y += LINE_HEIGHT;
                                rotateSpeedsElementsPos.y += LINE_HEIGHT;
                            }

                            extraHeight = EXTRA_HEIGHT + (LINE_HEIGHT * rotatePoints.arraySize);
                        }
                        else
                            extraHeight = EXTRA_HEIGHT;

                        

                        EditorGUI.PropertyField(rotatePointsPos, rotatePoints);
                        //EditorGUI.PropertyField(rotateSpeedsPos, rotateSpeeds);
                        break;
                    case (MovementType.BEZIER_CURVE):
                        Rect bezierTimePos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                        //setup percentages
                        //first 10% of the width = position.width * .1
                        //40%
                        Rect bezierEndLabelPosition = new Rect(position.x, position.y + 3 * LINE_HEIGHT, position.width * 0.1f, LINE_HEIGHT);
                        Rect bezierEndPos = new Rect(position.x + position.width * 0.1f, position.y + 3 * LINE_HEIGHT, position.width * 0.4f, LINE_HEIGHT);

                        Rect curvePointLabelPosition = new Rect(position.x + position.width * 0.5f, position.y + 3 * LINE_HEIGHT, position.width * 0.1f, LINE_HEIGHT);
                        Rect curvePointPos = new Rect(position.x + position.width * 0.6f, position.y + 3 * LINE_HEIGHT, position.width * 0.4f, LINE_HEIGHT);

                        SerializedProperty bezierEnd = property.FindPropertyRelative("bezierEndPoint");
                        SerializedProperty curvePoint = property.FindPropertyRelative("bezierCurvePoint");
                        SerializedProperty bezierPoint = property.FindPropertyRelative("bezierMoveTime");

                        EditorGUI.LabelField(bezierEndLabelPosition, "End Point: ");
                        EditorGUI.LabelField(curvePointLabelPosition, "Curve Point: ");
                        EditorGUI.PropertyField(bezierTimePos, bezierPoint);
                        EditorGUI.PropertyField(bezierEndPos, bezierEnd, GUIContent.none);
                        EditorGUI.PropertyField(curvePointPos, curvePoint, GUIContent.none);
                        
                        break;
                    case (MovementType.WAIT):
                        Rect waitPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                        SerializedProperty waitTime = property.FindPropertyRelative("waitTime");

                        EditorGUI.PropertyField(waitPos, waitTime);
                        break;
                    default:
                        break;
                        #endregion
                }
                break;
            case (WaypointTypes.FACING):
                //setup a position to display the variable
                Rect facingPos = new Rect(position.x + INDENT, position.y + LINE_HEIGHT, position.width - INDENT, LINE_HEIGHT);
                //get the variable off the script
                SerializedProperty facingType = property.FindPropertyRelative("facingTypes");
                //Display the property in the editor
                EditorGUI.PropertyField(facingPos, facingType);

                switch ((FacingType)facingType.enumValueIndex)
                {
                    #region "Facing"
                    case FacingType.FREE_MOVEMENT:
                        Rect freelookPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        SerializedProperty freelookTime = property.FindPropertyRelative("freeLookTime");
                        EditorGUI.PropertyField(freelookPos, freelookTime);
                        break;
                    case FacingType.LOOK_AND_RETURN:
                        Rect lookDestinationPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect waitTimePos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                        Rect travelToLabelPos = new Rect(position.x + 3*INDENT, position.y + 4 * LINE_HEIGHT, (position.width - 3* INDENT) * 0.2f, LINE_HEIGHT);
                        Rect travelToPos = new Rect(position.x + 3 * INDENT + (position.width - 3 * INDENT) * 0.2f, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT) * 0.3f, LINE_HEIGHT);
                        Rect travelFromLabelPos = new Rect(position.x + 3 * INDENT + (position.width - 3 * INDENT) * 0.5f, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT) * 0.2f, LINE_HEIGHT);
                        Rect travelFromPos = new Rect(position.x + 3 * INDENT + (position.width - 3 * INDENT) * 0.7f, position.y + 4 * LINE_HEIGHT, (position.width - 3 * INDENT) * 0.3f, LINE_HEIGHT);

                        SerializedProperty lookDestination = property.FindPropertyRelative("lookDestination");
                        SerializedProperty lookwait = property.FindPropertyRelative("lookTime");
                        SerializedProperty travelTo = property.FindPropertyRelative("travelToTime");
                        SerializedProperty travelFrom = property.FindPropertyRelative("travelBackTime");

                        EditorGUI.PropertyField(lookDestinationPos, lookDestination);
                        EditorGUI.PropertyField(waitTimePos, lookwait);
                        EditorGUI.LabelField(travelToLabelPos, "Time to: ");
                        EditorGUI.LabelField(travelFromLabelPos, "Time back: ");
                        EditorGUI.PropertyField(travelToPos, travelTo, GUIContent.none);
                        EditorGUI.PropertyField(travelFromPos, travelFrom, GUIContent.none);

                        break;
                    case FacingType.LOOK_CHAIN:
                        break;
                    case FacingType.FORCED_LOCATION:
                        Rect forcedDestinationPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect forcedTimePos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                        SerializedProperty forcedLookDestination = property.FindPropertyRelative("forcedLookTarget");
                        SerializedProperty forcedWait = property.FindPropertyRelative("forcedLookTime");

                        EditorGUI.PropertyField(forcedDestinationPos, forcedLookDestination);
                        EditorGUI.PropertyField(forcedTimePos, forcedWait);
                        break;
                    #endregion
                }
                break;
            case (WaypointTypes.EFFECTS):
                Rect effectPos = new Rect(position.x + INDENT, position.y + LINE_HEIGHT, position.width - INDENT, LINE_HEIGHT);

                SerializedProperty specialEffectsType = property.FindPropertyRelative("specialEffectsType");

                EditorGUI.PropertyField(effectPos, specialEffectsType);

                switch ((EffectsType)specialEffectsType.enumValueIndex)
                {
                    #region "Effects"
                    case EffectsType.CAMERA_SHAKE:
                        Rect intensityPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect shakeTimePos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                        SerializedProperty intensity = property.FindPropertyRelative("shakeIntensity");
                        SerializedProperty shakeTime = property.FindPropertyRelative("shakeTime");

                        EditorGUI.PropertyField(intensityPos, intensity);
                        EditorGUI.PropertyField(shakeTimePos, shakeTime);
                        break;
                    case EffectsType.FADE:
                        Rect fadeInPos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect fadeTimePos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);

                        SerializedProperty fadeIn = property.FindPropertyRelative("fadeOut");
                        SerializedProperty fadeTime = property.FindPropertyRelative("fadeTime");

                        EditorGUI.PropertyField(fadeInPos, fadeIn);
                        EditorGUI.PropertyField(fadeTimePos, fadeTime);
                        break;
                    case EffectsType.SPLATTER:
                        Rect splatterTimePos = new Rect(position.x + 2 * INDENT, position.y + 2 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect fadeInAndOutPos = new Rect(position.x + 2 * INDENT, position.y + 3 * LINE_HEIGHT, position.width - 2 * INDENT, LINE_HEIGHT);
                        Rect splatterFadeTimePos = new Rect(position.x + 3 * INDENT, position.y + 4 * LINE_HEIGHT, position.width - 3 * INDENT, LINE_HEIGHT);

                        SerializedProperty splatterTime = property.FindPropertyRelative("splatterTime");
                        SerializedProperty fadeInAndOut = property.FindPropertyRelative("fadeInAndOut");
                        SerializedProperty splatterFadeTime = property.FindPropertyRelative("splatterFadeTime");

                        EditorGUI.PropertyField(splatterTimePos, splatterTime);
                        EditorGUI.PropertyField(fadeInAndOutPos, fadeInAndOut);
                        if(fadeInAndOut.boolValue)
                        {
                            EditorGUI.PropertyField(splatterFadeTimePos, splatterFadeTime);
                        }
                        break;
                    #endregion
                }
                break;
            default:
                Debug.Log("Shit's broke yo");
                break;
        }

        #endregion
        EditorGUI.EndProperty();
    }

    public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
    {
        return base.GetPropertyHeight(property, label) + extraHeight;
    }
}
